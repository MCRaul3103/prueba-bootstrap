@library('prueba-shared-library')_

stage('Print Build Info'){
    printBuildinfo{
        name = "Sample Name"
    }
}
stage('Disable balancer'){
    disableBalancerUtils()
}
stage('Enable balancer'){
    enableBalancerUtils()
}
stage('Check Status'){
    checkStatus()
}